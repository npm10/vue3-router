import { validator } from './validator'
import { routes } from '../../../src/router'
const myFirstRoute = () => 'Hello! This is a my first route!' 

const route = [
    {
        path: '/vue3-router',
        component: () => 'Welcome to package vue3-router',
        auth: false
    },
    {
        path: '/my-first-route',
        component: myFirstRoute,
        auth: false
    },
    ...routes
]

const vue3Route = validator(route)

export {
    vue3Route
}