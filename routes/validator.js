const validator = (route) => {
    let routeCurrent = window.location.pathname
    let routeFound = route.find(item => item.path === routeCurrent)

    if (!routeFound) {
        return () => {
            return 'Route not Found!'
        }
    }

    if (routeFound.auth) {
        console.log('Route authenticate')
        /*
            SSO:
        */
    } 

    console.log('Route not authenticate')
    return routeFound.component
}

export {
    validator
}